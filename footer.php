<!-- Footer -->
<div class="footer">
  <a href="https://test.teumer-berlin.de/Chatty">Chatty - your small chatbot</a>
  -
  <a href="https://gitlab.com/Lumimaus">by Lumimaus</a>
  -
  <a id="count" class="center" href="https://teumer-berlin.de"></a>
</div>

<script>
// Copyright Year
// Update the count down every 1 second
var x = setInterval(function() {

  // Get the current year and isplay in "count"
  var current_year = new Date().getFullYear();
  document.getElementById("count").innerHTML = " © " + current_year + " teumer-berlin.de";

}, 1000);
</script>
