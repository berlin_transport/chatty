[![License](https://img.shields.io/badge/License-GNU%20General%20Public%20License%20v3.0-green.svg?logo=gnu)](https://www.gnu.org/licenses/gpl-3.0.en.html)

![HTML5](https://img.shields.io/badge/uses-HTML5-red.svg?logo=html5&color=e34f26) ![CSS](https://img.shields.io/badge/uses-CSS3-blue.svg?logo=css3&color=1572b6) ![JavaScript](https://img.shields.io/badge/uses-JavaScript-yellow.svg?logo=javascript&color=f7df1e) ![PHP](https://img.shields.io/badge/requires-PHP%207.0-blueviolet.svg?logo=php)
# Chatty
This is a quite simple project which allows you to run your own chatbot.
You are able to
* Enter your answers for the chatbot
* allow users to chat with your bot
* and find similar answers to your question.

## Requirements
You will need
* a web space with 10MB free storage
* a php instance which is able to deal with PDO
* a mySQL database with tables for the answers
* 10 minutes time.

## Installation
Put these files on your web space and modify the `config.php` document with your MySQL data.

Then you also have to setup a mySQL Database. Name of the main table should be "german" for now.
For further details on that look into the images.

[MySQL_columns_and_attributes](https://gitlab.com/berlin_transport/chatty/-/raw/master/MySQL_columns_and_attributes.png)

[MySQL_rows](https://gitlab.com/berlin_transport/chatty/-/raw/master/MySQL_rows.png)

Now you're ready.

# Notice
This project is a learning project for JavaScript, CSS, PHP and SQL.
The chatbot dosen't work as a AI and can give only answers given by you or your users.

I am NOT responsible for any abusive use!
