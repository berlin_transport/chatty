<?php
include("config.php");
session_start();

//Open mySQL connection
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  exit();
};

//Function to test a string for scripts
function test_input($term) {
  $term = trim($term);
  $term = stripslashes($term);
  $term = htmlspecialchars($term);
  return $term;
}

function remove_special_character($string) {
    $t = $string;
    $specChars = array(
        ' ' => ' ',    '!' => '',    '"' => '',
        '#' => '',    '$' => '',    '%' => '',
        '&' => '',    '\'' => '',   '(' => '',
        ')' => '',    '*' => '',    '+' => '',
        ',' => '',    '₹' => '',    '.' => '',
        '/-' => '',    ':' => '',    ';' => '',
        '<' => '',    '=' => '',    '>' => '',
        '?' => '',    '@' => '',    '[' => '',
        '\\' => '',   ']' => '',    '^' => '',
        '_' => '',    '`' => '',    '{' => '',
        '|' => '',    '}' => '',    '~' => '',
        '-----' => '',    '----' => '',    '---' => '',
        '/' => '',    '--' => '',   '/_' => '',
    );

    foreach ($specChars as $k => $v) {
        $t = str_replace($k, $v, $t);
    }
    return $t;
};

//Getting input from chatty.php; checking term
$term = $_REQUEST["q"];
$term = remove_special_character($term);
$term = test_input($term);
$term = strtolower($term);
settype($term, "string");
$origin = $term;
$list =  explode(" ", $term);

//Stirng length count; if strlen > 200 -> no SQL query
if (strlen($term) > 200) {
  echo "Der Satz ist leider zu lang, tut mir leid";
  mysqli_close($conn);
  exit;
};

//Stirng length count; if strlen < 4 -> no SQL query
if (strlen($term) < 4) {
  echo "Dieser Ausdruck ist leider zu kurz für mich, tut mir leid";
  mysqli_close($conn);
  exit;
};



//Word Count; if $i > 20 -´> no SQL query
$i = 0;
$nothing = Array ( );
$term2 = '';
$test = '';
while ($list != $nothing) {
  if ($i == 20) {
    echo "Der Satz ist mir zu kompliziert, tut mir leid";
    mysqli_close($conn);
    exit;
  } elseif ($i == 0) {
    $next = array_shift($list);
    $test = $next;
    $test = trim($test);
    //echo "[(test $i) $test] ";
    //language
    $sql1 = "SELECT * FROM `german` WHERE `input` LIKE '%".$test."%'";
    $result1 = mysqli_query($conn, $sql1);
    // Associative array
    $row1 = mysqli_fetch_assoc($result1);
    //echo "[".$i."]".$row1["output"];
    if ($row1["output"] == "") {
      $origin = "'".$origin."'";
      //language
      $sql3 = "INSERT INTO `german`(`input`,`output`) VALUES ($origin, 'empty')";
      $result3 = mysqli_query($conn, $sql3);
      break;
    } elseif ($row1["output"] == "empty"){
      continue;
    } else {
      $term2 = $next;
      $i = $i + 1;
    };

  } else {
    $next = array_shift($list);
    $test = $test." ".$next;
    $test = trim($test);
    //echo "[(test $i) $test] ";
    //language
    $sql4 = "SELECT * FROM `german` WHERE `input` LIKE '%".$test."%'";
    $result4 = mysqli_query($conn, $sql4);
    // Associative array
    $row4 = mysqli_fetch_assoc($result4);
    //echo "[".$i."]".$row4["output"];
    if ($row4["output"] == "") {
      $origin = "'".$origin."'";
      //language
      $sql5 = "INSERT INTO `german`(`input`,`output`) VALUES ($origin, 'empty')";
      $result5 = mysqli_query($conn, $sql5);
    } elseif ($row4["output"] == "empty"){
      continue;
    } else {
      $term2 = $term2." ".$next;
      $i = $i + 1;
    };
  };
};

$term2 = trim($term2);
//if ($term2 == ''){
//  $term2 = $origin;
//}

//echo "[".$term2."] ";
//language

if ($term2 != '') {
  $sql = "SELECT * FROM `german` WHERE `input` LIKE '%".$term2."%'";

  $result = mysqli_query($conn, $sql);
    // Associative array
    $row = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    if ($row["output"] == "") {
      echo "Es tut mir leid, dass verstehe ich nicht.";
      $origin = "'".$origin."'";
      //language
      $sql = "INSERT INTO `german`(`input`,`output`) VALUES ($origin, 'empty')";
      $result2 = mysqli_query($conn, $sql);
    } elseif ($row["output"] == "empty"){
      echo "Es tut mir leid, das verstehe ich noch nicht.";
    } else {
      echo ($row["output"]);
    };
} else {
  echo "Es tut mir leid, dass verstehe ich überhaupt nicht.";
};

mysqli_close($conn);
?>
