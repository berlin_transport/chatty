<!DOCTYPE html>
<!-- HTML Header -->
<html lang="de">
<title>Help Chatty</title>
<meta charset="utf-8">
<meta name="Description" content="Homepage">
<meta name="robots" content="noindex">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="/src/img/favicon.png">
<link rel="stylesheet" href="style.css">
<!-- HTML Header End -->

<div class="header">
  <h1>Chatty - Hilf Chatty schlauer zu werden</h1>
</div>

<div class="content round" id="div1">
  <p id="introtext"><b>Du bekommst gleich einen Satz oder eine Wortgruppe. Versuche diese möglichst authentisch zu beantworten. Das kann ernst sein, aber auch witzig oder einfach sympathisch. Bedenke bitte, der Chatbot ist keine Person und hat nur begrenzt die Möglichkeit Emotionen zu erwiedern oder zu zeigen. Bedenke dies bei deinen Antworten. Achte bitte auch auf deine Ausdrucksweise und eine logische Satzkonstruktion. Unlogische Symbole oder beleidigende Sätze werden nicht toleriert. Hier kann es zum Ausschluss per IP-Adresse kommen. Bist du nun bereit?</b></p>
	<button id="introbutton" class="btn" onclick="start()">Starten</button>
</div>

<?php
  include $_SERVER['DOCUMENT_ROOT']."/Chatty/footer.php";
?>

<script>

async function start() {
  var para = document.createElement("p");
	var introtext = document.getElementById("introtext");
	var introbutton = document.getElementById("introbutton");
	introtext.setAttribute("class","hidden");
	introbutton.setAttribute("class","hidden");

	var element = document.getElementById("div1");
  var child = document.getElementById("introtext");
  element.appendChild(para);

  para.setAttribute("id", "wait");
  para.setAttribute("class", "wait");
  var spinner = document.createElement("div");
  spinner.setAttribute("class","spinner");
  var bounce1 = document.createElement("div");
  bounce1.setAttribute("class","bounce1");
  var bounce2 = document.createElement("div");
  bounce2.setAttribute("class","bounce2");
  var bounce3 = document.createElement("div");
  bounce3.setAttribute("class","bounce3");
  spinner.appendChild(bounce1);
  spinner.appendChild(bounce2);
  spinner.appendChild(bounce3);
  para.appendChild(spinner);

  setTimeout(() => {
    console.log("start script done");
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if (this.responseText == "") {
        //output text
        var para = document.createElement("p");
        para.setAttribute("id", "out");
        para.setAttribute("class", "messageo");
        var node = document.createTextNode("Zurzeit gibt es keine neuen Antworten");
        para.appendChild(node);

        var element = document.getElementById("wait");
        element.parentNode.removeChild(element);
        var element = document.getElementById("div1");
        var child = document.getElementById("form");
        element.insertBefore(para,child);

        //back button
        var para = document.createElement("button");
        var node = document.createTextNode("Neu laden");
        para.setAttribute("id", "back");
        para.setAttribute("class","btn");
        para.setAttribute("onclick","window.location.reload(true)");
        para.appendChild(node);
        var child = document.getElementById("form");
        element.insertBefore(para, child);

        return;
      };
      //output text
      var para = document.createElement("p");
      para.setAttribute("id", "out");
      para.setAttribute("class", "messageo");
      var node = document.createTextNode("Chatty : " + this.responseText);
      window.textinput = this.responseText;
      para.appendChild(node);

      var element = document.getElementById("wait");
      element.parentNode.removeChild(element);
      var element = document.getElementById("div1");
      var child = document.getElementById("form");
      element.insertBefore(para,child);

      //input form
      var element = document.getElementById("div1");
      var para = document.createElement("form");
      para.setAttribute("id", "input");
      var node = document.createElement("input");
      node.setAttribute("type","text");
      node.setAttribute("name","text");
      node.setAttribute("id","chat");
      node.setAttribute("placeholder","Was würde Chatty antworten?");
      para.appendChild(node);

      var child = document.getElementById("form");
      element.insertBefore(para,child);

      //trash button
      var element = document.getElementById("div1");
      var para = document.createElement("button");
      var node = document.createTextNode("Dieser Text ist Schwachsinn");
      para.setAttribute("id", "trash");
      para.setAttribute("class","btn");
      para.setAttribute("onclick","trash()");
      para.appendChild(node);

      var child = document.getElementById("form");
      element.insertBefore(para, child);

      //trash button
      var element = document.getElementById("div1");
      var para = document.createElement("button");
      var node = document.createTextNode("Antwort einreichen");
      para.setAttribute("id", "send");
      para.setAttribute("class","btn");
      para.setAttribute("onclick","send()");
      para.appendChild(node);

      var child = document.getElementById("form");
      element.insertBefore(para, child);
      }
      };
      xmlhttp.open("GET", "getinput.php", true);
      xmlhttp.send();
    }, 2000);
};

async function input() {
    str = document.getElementById("chat").value;
    var out = document.getElementById("out");
    out.setAttribute("class","hidden");
    var form = document.getElementById("input")
    form.setAttribute("class","hidden")

    var para = document.createElement("p");
    para.setAttribute("id", "wait");
    para.setAttribute("class", "wait");
    var spinner = document.createElement("div");
    spinner.setAttribute("class","spinner");
    var bounce1 = document.createElement("div");
    bounce1.setAttribute("class","bounce1");
    var bounce2 = document.createElement("div");
    bounce2.setAttribute("class","bounce2");
    var bounce3 = document.createElement("div");
    bounce3.setAttribute("class","bounce3");
    spinner.appendChild(bounce1);
    spinner.appendChild(bounce2);
    spinner.appendChild(bounce3);
    para.appendChild(spinner);

    var element = document.getElementById("div1");
    var child = document.getElementById("input");
    element.insertBefore(para,child);

    setTimeout(() => {
      console.log("input script done");
      if (str.length === 0 && window.status === "send") {
        var para = document.createElement("p");
        para.setAttribute("id", "out");
        para.setAttribute("class", "messageo");
        var node = document.createTextNode("Du kannst eine Aussage nicht ohne Text beantworten!");
        para.appendChild(node);

        var element = document.getElementById("wait");
        element.parentNode.removeChild(element);
        var element = document.getElementById("div1");
        var child = document.getElementById("form");
        element.insertBefore(para,child);

        return;
      } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var para = document.createElement("p");
          para.setAttribute("id", "out");
          para.setAttribute("class", "messageo");

          if (this.responseText == "True") {
            var node = document.createTextNode("Danke für deine Hilfe");
            para.appendChild(node);

            var element = document.getElementById("wait");
            element.parentNode.removeChild(element);
            var element = document.getElementById("div1");
            var child = document.getElementById("form");
            element.insertBefore(para,child);

          } else {
            var node = document.createTextNode("Es ist ein Fehler aufgetreten");
            console.log(this.responseText);
            para.appendChild(node);

            var element = document.getElementById("wait");
            element.parentNode.removeChild(element);
            var element = document.getElementById("div1");
            var child = document.getElementById("form");
            element.insertBefore(para,child);
          }
          //back button
          var para = document.createElement("button");
          var node = document.createTextNode("Seite neu laden");
          para.setAttribute("id", "back");
          para.setAttribute("class","btn");
          para.setAttribute("onclick","window.location.reload(true)");
          para.appendChild(node);
          var child = document.getElementById("form");
          element.insertBefore(para, child);

          //document.getElementById("txtHint").innerHTML = this.responseText;
        }
        };
         var params = "input=" + window.textinput + "&" + "response=" + str + "&" + "status=" + window.status;
         xmlhttp.open("GET", "getoutput.php?"+ params, true);
         xmlhttp.send();
       } }, 2000);
}

async function trash() {
  window.status = "trash"
  var trash = document.getElementById("trash");
  var send = document.getElementById("send");
  trash.parentNode.removeChild(trash);
  send.parentNode.removeChild(send);

  //sure button
  var element = document.getElementById("div1");
  var para = document.createElement("button");
  var node = document.createTextNode("Sicher ?");
  para.setAttribute("id", "sure");
  para.setAttribute("class","btn");
  para.setAttribute("onclick","sure()");
  para.appendChild(node);
  var child = document.getElementById("form");
  element.insertBefore(para, child);

  //back button
  var para = document.createElement("button");
  var node = document.createTextNode("Zurück");
  para.setAttribute("id", "back");
  para.setAttribute("class","btn");
  para.setAttribute("onclick","window.location.reload(true)");
  para.appendChild(node);
  var child = document.getElementById("form");
  element.insertBefore(para, child);
}
async function send() {
  window.status = "send"
  var trash = document.getElementById("trash");
  var send = document.getElementById("send");
  trash.parentNode.removeChild(trash);
  send.parentNode.removeChild(send);

  //sure button
  var element = document.getElementById("div1");
  var para = document.createElement("button");
  var node = document.createTextNode("Sicher ?");
  para.setAttribute("id", "sure");
  para.setAttribute("class","btn");
  para.setAttribute("onclick","sure()");
  para.appendChild(node);
  var child = document.getElementById("form");
  element.insertBefore(para, child);

  //back button
  var para = document.createElement("button");
  var node = document.createTextNode("Zurück");
  para.setAttribute("id", "back");
  para.setAttribute("class","btn");
  para.setAttribute("onclick","window.location.reload(true)");
  para.appendChild(node);
  var child = document.getElementById("form");
  element.insertBefore(para, child);

}
async function sure() {
  var sure = document.getElementById("sure");
  var back = document.getElementById("back");
  sure.parentNode.removeChild(sure);
  back.parentNode.removeChild(back);
  console.log(window.status);
  input()
}


// Copyright Year
// Update the count down every 1 second
var x = setInterval(function() {

  // Get the current year and isplay in "count"
  var current_year = new Date().getFullYear();
  document.getElementById("count").innerHTML = " © " + current_year + " teumer-berlin.de";

}, 1000);
</script>

</body>
</html>
