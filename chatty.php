<!DOCTYPE html>
<!-- HTML Header -->
<html lang="de">
<title>Chatty</title>
<meta charset="utf-8">
<meta name="Description" content="Homepage">
<meta name="robots" content="noindex">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="/src/img/favicon.png">
<link rel="stylesheet" href="style.css">
<!-- HTML Header End -->

<div class="header">
  <h1>Chatty - Dein virtueller Chatbot</h1>
</div>

<div class="content round" id="div1">
  <p><b>Beginne mit Chatty unten zu schreiben, Chatty wird versuchen dir zu antworten:</b></p>
  <!--<p id="chatty">Chatty: <span id="txtHint"></span></p>-->
  <form id="form">
  <input type="text" name="text" id="chat" onkeypress="enterMessage(event)" placeholder="Was möchtest du mir sagen ...?">
  </form>
  <p>Drücke Enter zum Absenden</p>
</div>

<?php
  include $_SERVER['DOCUMENT_ROOT']."/Chatty/footer.php";
?>

<script>

//XMLHttpRequest Handling Tool
async function enterMessage(e) {
  if(e.keyCode === 13){
    e.preventDefault(); // Ensure it is only this code that runs
    str = document.getElementById("chat").value;
    document.getElementById('chat').value = '';
    var para = document.createElement("p");
    para.setAttribute("id", "in");
    para.setAttribute("class", "messagei");
    var node = document.createTextNode("Ich : " + str);
    para.appendChild(node);

    var element = document.getElementById("div1");
    var child = document.getElementById("form");
    element.insertBefore(para,child);

    var para = document.createElement("p");
    para.setAttribute("id", "wait");
    para.setAttribute("class", "wait");
    var spinner = document.createElement("div");
    spinner.setAttribute("class","spinner");
    var bounce1 = document.createElement("div");
    bounce1.setAttribute("class","bounce1");
    var bounce2 = document.createElement("div");
    bounce2.setAttribute("class","bounce2");
    var bounce3 = document.createElement("div");
    bounce3.setAttribute("class","bounce3");
    spinner.appendChild(bounce1);
    spinner.appendChild(bounce2);
    spinner.appendChild(bounce3);
    para.appendChild(spinner);

    var element = document.getElementById("div1");
    var child = document.getElementById("form");
    element.insertBefore(para,child);

    setTimeout(() => {
      console.log("timeout");

      if (str.length == 0) {
        var para = document.createElement("p");
        para.setAttribute("id", "out");
        para.setAttribute("class", "messageo");
        var node = document.createTextNode("Chatty : " + "Was meinst du?");
        para.appendChild(node);

        var element = document.getElementById("wait");
        element.parentNode.removeChild(element);
        var element = document.getElementById("div1");
        var child = document.getElementById("form");
        element.insertBefore(para,child);

        return;
      } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var para = document.createElement("p");
          para.setAttribute("id", "out");
          para.setAttribute("class", "messageo");
          var node = document.createTextNode("Chatty : " + this.responseText);
          para.appendChild(node);

          var element = document.getElementById("wait");
          element.parentNode.removeChild(element);
          var element = document.getElementById("div1");
          var child = document.getElementById("form");
          element.insertBefore(para,child);
          //document.getElementById("txtHint").innerHTML = this.responseText;
        };
        };
         xmlhttp.open("GET", "gethint.php?q=" + str, true);
         xmlhttp.send();
       }; }, 2000);
};
};
</script>

</body>
</html>
