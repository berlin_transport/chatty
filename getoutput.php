<?php
include("config.php");
session_start();

//Open mySQL connection
$conn = mysqli_connect($db_hostname,$db_username,$db_password,$db_database);
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  exit();
};

//Function to test a string for scripts
function test_input($term) {
  $term = trim($term);
  $term = stripslashes($term);
  $term = htmlspecialchars($term);
  return $term;
}

//Getting input from chatty.php; checking term
$input = $_GET["input"];
$term = $_GET["response"];
$status = $_GET["status"];

$term = test_input($term);
settype($term, "string");

$input = test_input($input);
settype($input, "string");

//Stirng length count; if strlen > 90 -> no SQL query
if (strlen($term) > 90) {
  echo "Der Satz ist leider zu lang, tut mir leid";
  mysqli_close($conn);
  exit;
};

//word Count
if (str_word_count($term) > 10) {
  echo "False";
  mysqli_close($conn);
  exit;
};

if ($status == "send") {
  //language
  $sql = "UPDATE `german` SET `output`='$term' WHERE `input` LIKE '$input'";
} else if ($status == "trash") {
  $sql = "DELETE FROM `german` WHERE `input` LIKE '$input'";
} else {
  echo "MySQL Error";
  exit;
}

$result = mysqli_query($conn, $sql);
// Associative array
$row = mysqli_fetch_assoc($result);
mysqli_free_result($result);

echo "True";
mysqli_close($conn);
?>
